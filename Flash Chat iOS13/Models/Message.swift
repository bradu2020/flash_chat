//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Radu Baloi on 19/12/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String //email of sender
    let body: String
}
