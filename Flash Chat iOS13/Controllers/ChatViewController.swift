//
//  ChatViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit
import Firebase

class ChatViewController: UIViewController {
    
    //create referece to database
    let db = Firestore.firestore()
    
//    var messages: [Message] = [
//        Message(sender: "1@2.com", body: "Hey"),
//        Message(sender: "2@2.com", body: "222"),
//        Message(sender: "3@2.com", body: "333")
//    ]
    
    var messages: [Message] = []

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //needed after extension UITableViewDataSource to trigger the delegate methods
        tableView.dataSource = self

//        //Set delegate to self for UITableViewDelegate
//        tableView.delegate = self
        
        //show title in the middle of navbar
        title = K.appName
        //remove when back btn when user is logged in
        navigationItem.hidesBackButton = true
        
        //To use a custom design file nibName is the .xib .swift, register it
        tableView.register(UINib(nibName: K.cellNibName, bundle: nil), forCellReuseIdentifier: K.cellIdentifier)
    
        loadMessages()
    }
    
    //to read messages https://firebase.google.com/docs/firestore/quickstart
    func loadMessages() {
//        messages = [] //this results in duplicating all msgs but the last
        
        //get documents needs to be called every single time to get latest documents added
        //db.collection(K.FStore.collectionName).getDocuments { (querySnapshot, error) in
            
        //https://firebase.google.com/docs/firestore/query-data/listen
        //new msg added to db -> block of code gets triggered again
         db.collection(K.FStore.collectionName)
            .order(by: K.FStore.dateField) //to order messages
            .addSnapshotListener { (querySnapshot, error) in
            self.messages = [] //empty the arr in closure each time  + self.
            if let e = error {
                print("Issue retrieving data from firestore. \(e)")
            } else {
                //https://firebase.google.com/docs/reference/swift/firebasefirestore/api/reference/Classes/QuerySnapshot
                // first element,data dict with key senderField: querySnapshot?.documents[0].data(K.FStore.senderField)
                
                //get unwrapped array of snapshot documents
                if let snapshotDocuments = querySnapshot?.documents {
                    for doc in snapshotDocuments {
                        //print(doc.data())
                        let data = doc.data()
                        
                        //because we use a string to get item from dict -> we get back an optional with Any? dataType (fireBase fault)
                        //downcast to have an optional string + if let to bind
                        if let messageSender = data[K.FStore.senderField] as? String, let messageBody = data[K.FStore.bodyField] as? String {
                            //now that both are normal strings
                            let newMessage = Message(sender: messageSender, body: messageBody)
                            //self. inside closures
                            self.messages.append(newMessage)
                            
                            //when manipulating UI inside a closure: get a hold of the main queue (thread)
                            DispatchQueue.main.async {
                                //still unable to see msg in app -> trigger dataSource methods again
                                self.tableView.reloadData()
                                
                                //to always show the latest msg //self. in closure //only one section
                                let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
                                self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                            }

                        }
                    }
                }
            }
        }
    }
    
    @IBAction func sendPressed(_ sender: UIButton) {
        //both are optional strings text could  be empty, user could be logged out
        //firebase, doc, ios, manage users: + bind both optionals to constants if not nil
        if let messageBody = messageTextfield.text, let messageSender =
            Auth.auth().currentUser?.email {
            //cloud firestore get started add data //Date object to get timestamp and order messages
            db.collection(K.FStore.collectionName).addDocument(data: [K.FStore.senderField: messageSender,
                                                                      K.FStore.bodyField: messageBody,
                                                                      K.FStore.dateField: Date().timeIntervalSince1970]) { (error) in
                                                                        if let e = error {
                                                                            print("There was an issue saving data to firestore, \(e)")
                                                                        } else {
                                                                            print("Succesfully saved data")
                                                                            
                                                                            //ensure this happens in the main thread //self. in closure //closures usually happen in other threads
                                                                            DispatchQueue.main.async {
                                                                                self.messageTextfield.text = ""
                                                                            }
                                                                        }
            }
        }
    }
    
    @IBAction func logOutPressed(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
            //nav user to welcome screen
            navigationController?.popToRootViewController(animated: true)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
      
    }
    
} //outside of ChatViewController

//MARK: - UITableViewDataSource
//when tableView loads up its gona make a request for data
extension ChatViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //as many cells as msgs in array
        return messages.count
    } //returns 3
    
    //this is called 3 times
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        //create a cell and return it to the tableView
//        let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath)
        
        
        let message = messages[indexPath.row]
        
        //To be able to access properties from MessageCell.xib downcast with as!
                let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! MessageCell
                
        //        //access the number of each row "\(indexPath.row)"
        //        cell.textLabel?.text = messages[indexPath.row].body
        //        //delete from main storyboard the reusable cell
                
                cell.label.text = message.body
        
        //message from current user, show right Image and color
        if message.sender == Auth.auth().currentUser?.email {
            cell.leftImageView.isHidden = true
            cell.rightImageView.isHidden = false
            cell.messageBubble.backgroundColor = UIColor(named: K.BrandColors.lightPurple)
            cell.label.textColor = UIColor(named: K.BrandColors.purple)
        } else {
            cell.leftImageView.isHidden = false
            cell.rightImageView.isHidden = true
            cell.messageBubble.backgroundColor = UIColor(named: K.BrandColors.purple)
            cell.label.textColor = UIColor(named: K.BrandColors.lightPurple)
        }
        
        return cell    }
}

////MARK: - UITableViewDelegate
////whenever the tableView is interacted with by the user (ex: selected) ; storyboard cell selection: default
//extension ChatViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath.row)
//    }
//}
