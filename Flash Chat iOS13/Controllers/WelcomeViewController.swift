//
//  WelcomeViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit


class WelcomeViewController: UIViewController {

    //using the imported file after storyboard->identity-> CLTypingLabel
    @IBOutlet weak var titleLabel: UILabel!
    
    //just before view shows up on screen
    override func viewWillAppear(_ animated: Bool) {
        
        //always call super when overriding functions from the super class
        super.viewWillAppear(animated)
        //remove nav bar on welcome screen
        navigationController?.isNavigationBarHidden = true
        //has side effect to remove it from views that come after this one SO:
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //always call super when overriding functions from the super class
        super.viewWillDisappear(animated)
        //SO: when this view disappears,show again the nav bar on next views
        navigationController?.isNavigationBarHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        titleLabel.text = ""
        var charIndex = 0.0
        let titleText = K.appName
        for letter in titleText{
            //explicit self.  inside closures
            Timer.scheduledTimer(withTimeInterval: 0.1 * charIndex, repeats: false) { (timer) in
                self.titleLabel.text?.append(letter)
            }
            charIndex += 1
        }
       
    }
    

}
