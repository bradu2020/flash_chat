//
//  MessageCell.swift
//  Flash Chat iOS13
//
//  Created by Radu Baloi on 20/12/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

//Cocoa Touch Class + Tick "Create xib file"
import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet weak var messageBubble: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var leftImageView: UIImageView!
    
    //called when a new cell is created from MessageCell.xib
    override func awakeFromNib() {
        //similar to viewDidLoad in ViewControllers

        super.awakeFromNib()
        
        //corner radius growing with the height of the msg
        messageBubble.layer.cornerRadius = messageBubble.frame.size.height / 5
        //by defult labels have lines 1, set lines 0 to let it and its view to grow
        //stackView aligment top to keep the avatar on top (not in the middle)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
